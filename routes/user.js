import Router from 'koa-router'
import svgCaptcha from 'svg-captcha'
import session from 'koa-session'
import { v4 as uuidv4 } from 'uuid'
import multiparty from 'multiparty'
import jwt from 'jsonwebtoken'
import * as userService from '../services/user.js'
import { secret } from '../config/secret.js'
export const router = new Router()

// 获取图行验证码
router.get('/api/v1/auth/captcha', async (ctx) => {
  const captcha = svgCaptcha.create({
    inverse: false, // 翻转颜色
    fontSize: 48, // 字体大小
    noise: 4, // 干扰线条数
    width: ctx.query.width || 150, // 宽度
    height: ctx.query.height || 50, // 高度
    size: 4, // 验证码长度
    ignoreChars: '0o1i', // 验证码字符中排除 0o1i
    color: true, // 验证码是否有彩色
    background: '#cc9966' // 验证码图片背景颜色
  })
  let sid = uuidv4() // ⇨ '9b1deb4d-3b7d-4bad-9bdd-2b0d7b3dcb6d'
  // 保存到cookie,忽略大小写
  // ctx.cookies.set('sid', captcha.text)
  // 将获取到的 验证码文本进行转为小写
  // 存储到 session 里
  session.imgcode = captcha.text.toLowerCase()
  // ctx.cookies.set('sid', sid, {
  //   httpOnly: true
  // })
  // 设置响应头
  // ctx.response.type = 'image/jpeg'
  ctx.body = {
    code: '00000',
    data: {
      captchaBase64: captcha.data,
      sid: sid
    }
  }
})

// 注册表单页
router.get('/register', async function (ctx) {
  await ctx.render('register')
})
// 注册处理
router.post('/register', async function (ctx) {
  const data = ctx.request.body

  if (!data.username || !data.password || !data.confirmpassword) {
    ctx.throw(400, '您的请求有误')
  }

  if (data.password !== data.confirmpassword) {
    ctx.throw(400, '您的两次密码不一样')
  }
  const logged = await userService.register(data.username, data.password)

  if (!logged) {
    ctx.throw(400, '注册失败 — 账号重复')
  }
  ctx.cookies.set('logged', 1, { signed: true, httpOnly: true })
  ctx.cookies.set(
    'username',
    Buffer.from(data.username, 'utf8').toString('base64'),
    { httpOnly: true }
  )
  ctx.redirect('/', '注册成功')
})

// 注销
router.get('/registerout', async function (ctx) {
  const logged = await userService.registerout(ctx.state.username)
  ctx.cookies.set('logged', 0, {
    maxAge: -1,
    signed: true
  })
  ctx.cookies.set('username', 0, {
    maxAge: -1
  })
  ctx.redirect('/', '注销成功')
})

// 登录表单页
router.get('/login', async function (ctx) {
  await ctx.render('login')
})

// 登录处理
router.post('/login', async function (ctx) {
  const data = ctx.request.body

  if (!data.username || !data.password) {
    ctx.throw(400, '您的请求有误')
  }

  const userInfo = await userService.login(data.username, data.password)
  if (userInfo?.type) {
    ctx.throw(400, userInfo.type)
  }

  ctx.cookies.set('logged', 1, { signed: true, httpOnly: true })

  // cookies不能设置中文明文,编码后
  ctx.cookies.set(
    'username',
    Buffer.from(userInfo.username, 'utf-8').toString('base64'),
    { httpOnly: true }
  )

  ctx.redirect('/', '登陆成功')
})

/* 获取一个期限为4小时的token */
function getToken(payload = {}) {
  return jwt.sign(payload, secret, { expiresIn: '4h' })
}

// 登录处理
router.post('/api/v1/auth/login', async function (ctx) {
  const data = ctx.request.body
  if (!data.username || !data.password) {
    // ctx.throw(400, '您的请求有误')
    ctx.body = {
      code: 10008,
      msg: '用户名或密码不能为空'
    }
  }
  const userInfo = await userService.login(data.username, data.password)
  if (userInfo?.type) {
    ctx.body = {
      code: 10009,
      msg: '用户名或密码错误'
    }
  } else {
    // cookies不能设置中文明文,编码后
    ctx.cookies.set('token', userInfo.token, { httpOnly: true })
    ctx.body = {
      code: 200,
      msg: '登录成功',
      data: {
        tokenType: 'Bearer',
        accessToken: userInfo.token.split(' ')[1]
      }
    }
  }
  // ctx.redirect('/', '登陆成功')
})

// 退出登录
router.get('/logout', async function (ctx) {
  ctx.cookies.set('logged', 0, {
    maxAge: -1,
    signed: true
  })
  ctx.cookies.set('username', 0, {
    maxAge: -1
  })
  ctx.redirect('/', '退出登录成功')
})
