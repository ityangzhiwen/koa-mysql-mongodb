import Router from 'koa-router'
import * as postService from '../services/post.js'
import * as userService from '../services/user.js'
import { promisePool } from '../database/mysql2/index.js'

export const router = new Router()

router.get('/', async function (ctx) {
  const list = await postService.list()
  const treeList = await userService.getList()
  await ctx.render('index', {
    list,
    treeList
  })
})

router.get('/add', async function (ctx) {
  const list = await postService.list()
  ctx.body = list
  // await ctx.render('index', { list })
})
