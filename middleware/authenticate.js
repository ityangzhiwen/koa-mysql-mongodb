export async function authenticate(ctx, next) {
  const logged = ctx.cookies.get('logged', { signed: true })
  const username = ctx.cookies.get('username')
  ctx.state.logged = !!logged
  ctx.state.username =
    username && Buffer.from(username, 'base64').toString('utf-8')
  await next()
}
