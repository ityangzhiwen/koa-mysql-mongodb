/** @描述 自增加id */

export async function autoIncreaseId(tb) {
  let docs = await tb.find().toArray()
  console.log(`docs => %O `, docs)
  let maxId = 0
  if (docs.length) {
    maxId = Math.max(...docs.map((doc) => doc.id))
  }
  console.log('maxId=', maxId)
  return maxId + 1
}
