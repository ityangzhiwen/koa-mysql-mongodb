class ErrorTypeModule extends Error {
  constructor(status, errorCode, msg) {
    // 使用父类的构造函数
    super()
    // 状态码
    this.statusCode = status
    // 错误码
    this.errorCode = errorCode
    // 错误信息
    this.msg = msg
  }
}

// 异常处理
const handleError = async (ctx, next) => {
  try {
    // 无错误继续下一步
    await next()
  } catch (error) {
    // 此处就可以判断是否是异常基类，如果是表示就是一个已知型的错误
    if (error instanceof ErrorTypeModule) {
      // 错误路径可以在这里获取添加
      error.requestUrl = `${ctx.request.method} ${ctx.request.url}`
      // 设置 http 状态码
      ctx.status = error.statusCode
      // 返回整个错误信息
      ctx.body = error
    }
  }
}

export { ErrorTypeModule, handleError }
