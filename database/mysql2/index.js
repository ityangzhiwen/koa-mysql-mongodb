import mysql from 'mysql2'
let databaseType = process.env.databaseType

let config = {
  host: 'localhost',
  port: '3306',
  user: 'root',
  database: 'qcms',
  password: '123456',
  // waitForConnections: true,
  // connectionLimit: 10,
  // queueLimit: 0,
  // enableKeepAlive: true,
  // keepAliveInitialDelay: 0,
  rowsAsArray: false
}
if (databaseType === 'insCode') {
  config = {
    host: 'mysql.inscode.run',
    port: '3306',
    user: 'root',
    password: 'inscode',
    database: 'post_db'
    // waitForConnections: true,
    // connectionLimit: 10,
    // queueLimit: 0,
    // enableKeepAlive: true,
    // keepAliveInitialDelay: 0,
    // rowsAsArray: false
  }
}
// 常规
// const connection = mysql.createConnection({
//   host: 'localhost',
//   user: 'root',
//   database: 'postDB'
// })

// 连接池的使用
const pool = mysql.createPool(config)

export const promisePool = pool.promise()

promisePool.execute(`
CREATE TABLE
    IF NOT EXISTS user_tb(
        id INT NOT NULL AUTO_INCREMENT,
        username VARCHAR(100) NOT NULL,
        password VARCHAR(100) NOT NULL,
        date DATE,
        token VARCHAR(200) NOT NULL,
        PRIMARY KEY (id)
    )ENGINE = InnoDB DEFAULT CHARSET = utf8;
`)

promisePool.execute(`
CREATE TABLE
    IF NOT EXISTS post_tb(
        id INT NOT NULL AUTO_INCREMENT,
        title VARCHAR(100) NOT NULL,
        content VARCHAR(100) NOT NULL,
        time DATE,
        PRIMARY KEY (id)
    )ENGINE = InnoDB DEFAULT CHARSET = utf8;
`)
